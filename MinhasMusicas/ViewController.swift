//
//  ViewController.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Character : Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var CharacterList:[Character] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.CharacterList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let character = self.CharacterList[indexPath.row]
        print(character)
        
        cell.name.text = character.name
        cell.nameActor.text = character.actor
        cell.imageCharacter.kf.setImage(with: URL(string: character.image))
        
        return cell
    }
 
    @IBOutlet weak var tableView: UITableView!
    
    func getNewCharacter(){
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Character].self) { response in
                        if let character = response.value {
                            self.CharacterList = character
                        }
            self.tableView.reloadData()

                    }
            }

    override func viewDidLoad() {
            super.viewDidLoad()
            
            self.tableView.dataSource = self
            self.tableView.delegate = self
            
        getNewCharacter()
        }



}

